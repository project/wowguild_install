<?php
/**
 * @file
 * Providing extra functionality for the MMOToon UI via views.
 */


/**
 *
 * Implements hook_views_default_views_alter().
 *
 * Alter the default calendar view to only show guild events and place on the main-menu.  Sometimes this gets called before calendar.module?
 *
 * @param array $views
 */
function wowguild_install_views_default_views_alter($views) {
  
  if (isset($views['calendar'])) {
    /* Filter criterion: Content: Type */
    $views['calendar']->display['default']->display_options['filters']['type']['id'] = 'type';
    $views['calendar']->display['default']->display_options['filters']['type']['table'] = 'node';
    $views['calendar']->display['default']->display_options['filters']['type']['field'] = 'type';
    $views['calendar']->display['default']->display_options['filters']['type']['value'] = array(
          'guild_event' => 'guild_event',
    );
    
    // Month Display.  Change menu to place calendar in main menu
    $views['calendar']->display['page_1']->display_options['path'] = 'calendar/month';
    $views['calendar']->display['page_1']->display_options['menu']['type'] = 'normal';
    $views['calendar']->display['page_1']->display_options['menu']['title'] = 'Calendar';
    $views['calendar']->display['page_1']->display_options['menu']['weight'] = '10';
    $views['calendar']->display['page_1']->display_options['menu']['name'] = 'main-menu';
    $views['calendar']->display['page_1']->display_options['menu']['context'] = 0;
    $views['calendar']->display['page_1']->display_options['tab_options']['type'] = 'normal';
    $views['calendar']->display['page_1']->display_options['tab_options']['title'] = 'Calendar';
    $views['calendar']->display['page_1']->display_options['tab_options']['weight'] = '10';
  }
  return $views;
}

/**
 * Implement hook_views_default_views().
 */
function wowguild_install_views_default_views() {
  $views = array();
  
  $view = new view;
  $view->name = 'guild_calendar';
  $view->description = 'Guild Calendar - Requires FullCalendar module';
  $view->tag = 'wowguild';
  $view->base_table = 'node';
  $view->human_name = 'Guild Calendar';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = module_exists('fullcalendar')?FALSE:TRUE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Calendar';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'fullcalendar';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: time */
  $handler->display->display_options['fields']['field_time']['id'] = 'field_time';
  $handler->display->display_options['fields']['field_time']['table'] = 'field_data_field_time';
  $handler->display->display_options['fields']['field_time']['field'] = 'field_time';
  $handler->display->display_options['fields']['field_time']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_time']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_time']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_time']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_time']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_time']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_time']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_time']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_time']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_time']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_time']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'guild_event' => 'guild_event',
  );
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'fullcalendar';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Calendar';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  
  
  
  
  $views[$view->name] = $view;
  
  return $views;

}
