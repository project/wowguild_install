(function ($) {
	Drupal.Wowguildsite = Drupal.Wowguildsite || {};
    var toon;
    var WowguildsiteLoading = false;
    var WowguildsiteLastName = '';
    var WowguildsiteLastRealm = '';
    var WowguildsiteRequestUrl = '/api/wow/character/';
    
    Drupal.Wowguildsite.validateToon = function (fullrealm,name) {
      if(name.length >= 3 && WowguildsiteLoading == false && (WowguildsiteLastName != name || WowguildsiteLastRealm != fullrealm)) {
        for(var i=0;i<all_realms.length;i++) {
          if(all_realms[i] == fullrealm) {
          
            var zone = all_realms[i].substr(all_realms[i].length-2).toLowerCase();
            var realm = all_realms[i].substr(0, all_realms[i].length-3);
            var server = '';
            for(var key in all_regions) {
              if (key == zone) {
                server = all_regions[key];
              }
            }
            var url = 'http://' + server + WowguildsiteRequestUrl + realm + '/' + name + '?fields=guild';
            $('#wowguildsiteout').html('<div class=\'messages\'><h2 class=\'element-invisible\'>Message</h2>Validating ' + name + ' on ' + fullrealm + ' [<a href=\"' + url + '\">armory link</a>]</div>');
            WowguildsiteLoading = true;
            WowguildsiteLastName = name;
            WowguildsiteLastRealm = fullrealm;
            
            
            $.jsonp({
              url: url,
              callbackParameter: 'jsonp', // Required by battlenet.
              success: function(data) {
                var output = '';
              	if (data.guild != null) {
                  toon = data;
                  
                  output += 'Found ' + toon.name + ' of ' + toon.guild.name + ' <br />';
                  output += '<img src=\'http://' + server + '/static-render/' + zone + '/' + toon.thumbnail + '\' /><br />';
  								
					// Set Username and Site name
					$('#edit-account-name').val(toon.name);
					$('#edit-site-name').val(toon.guild.name);
                }
                else {
                  output += 'Error ' + data.name + ' is unguilded!<br />';
                  output += '<img src=\'http://' + server + '/static-render/' + zone + '/' + data.thumbnail + '\' /><br />';
                }
                $('#wowguildsiteout').html('<div class=\'messages\'><h2 class=\'element-invisible\'>Message</h2>' + output + '</div>');
                WowguildsiteLoading = false;
              },
              error: function (xOptions, textStatus) {
                $('#wowguildsiteout').html('<div class=\'messages\'><h2 class=\'element-invisible\'>Message</h2>Error: ' + textStatus + '<br />called url: <a href=\"' + xOptions.url + '\">' + xOptions.url + '</a></div>');
                WowguildsiteLoading = false;
              }
			});
						
						
            
          }
        }
      }
    };
    
    Drupal.Wowguildsite.autoAttachValidateToon = function() {
      $('#edit-character').append('<div id=\"wowguildsiteout\"></div>');
      $('#edit-name').change(function() {
        Drupal.Wowguildsite.validateToon($('#edit-realm').val(), $('#edit-name').val());
      });
      $('#edit-realm').autocomplete({ source: all_realms });
      $('#edit-realm').blur(function() {
        Drupal.Wowguildsite.validateToon($('#edit-realm').val(), $('#edit-name').val());
      });
    };
    $(Drupal.Wowguildsite.autoAttachValidateToon);
  })(jQuery);