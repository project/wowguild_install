<?php
// $Id:

/**
* Implements hook_form_FORM_ID_alter().
*/
//function wowguild_install_form_alter(&$form, &$form_state) { // _install_select_locale_form  // install_select_locale_form
/*
function wowguild_install_form_alter(&$form, &$form_state) {
  $form['default language'] = array(
        '#type' => 'fieldset',
        '#title' => 'Default Language Settings',
        '#collapsible' => TRUE,
        '#description' => st('<em>Note</em>: Changes will not display until characters are updated.'),
        '#weight' => 0
  );
  $form['default language']['wowguild_default_language'] = array(
        '#type' => 'radios',
        '#title' => st('wowguild selector language'),
        '#description' => st('French, German and Russian languages are only available on EU realms.'),
        '#options' => array(
          'en' => st('English'),
          'pl' => st('Portuguese'),
          'fr' => st('French'),
          'es' => st('Spanish'),
          'de' => st('German'),
          'ru' => st('Russian'),
        ),
        '#default_value' => 'en',
  );
  }
function wowguild_install_form_install_select_locale_form_alter(&$form, &$form_state) {
  $form['default language'] = array(
        '#type' => 'fieldset',
        '#title' => 'Default Language Settings',
        '#collapsible' => TRUE,
        '#description' => st('<em>Note</em>: Changes will not display until characters are updated.'),
        '#weight' => 0
  );
  $form['default language']['wowguild_default_language'] = array(
        '#type' => 'radios',
        '#title' => st('wowguild selector language'),
        '#description' => st('French, German and Russian languages are only available on EU realms.'),
        '#options' => array(
          'en' => st('English'),
          'pl' => st('Portuguese'),
          'fr' => st('French'),
          'es' => st('Spanish'),
          'de' => st('German'),
          'ru' => st('Russian'),
        ),
        '#default_value' => 'en',
  );
}
*/


/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function wowguild_install_form_install_configure_form_alter(&$form, &$form_state) {
  
  $all_regions = array(
          'us' => 'us.battle.net',
          'eu' => 'eu.battle.net',
          'kr' => 'kr.battle.net',
          'tw' => 'tw.battle.net',
          'cn' => 'battlenet.com.cn'
  );
  
  $const_realms_list = array(
        "Aegwynn-US", "Aerie Peak-US", "Agamaggan-US", "Aggramar-US", "Akama-US", "Alexstrasza-US", "Alleria-US", "Altar of Storms-US", "Alterac Mountains-US", "Aman'thul-US", "Andorhal-US", "Anetheron-US", "Antonidas-US", "Anub'arak-US", "Anvilmar-US", "Arathor-US", "Archimonde-US", "Area 52-US", "Argent Dawn-US", "Arthas-US", "Arygos-US", "Auchindoun-US", "Azgalor-US", "Azjol-Nerub-US", "Azshara-US", "Azuremyst-US", "Baelgun-US", "Balnazzar-US", "Barthilas-US", "Black Dragonflight-US", "Blackhand-US", "Blackrock-US", "Blackwater Raiders-US", "Blackwing Lair-US", "Blade's Edge-US", "Bladefist-US", "Bleeding Hollow-US", "Blood Furnace-US", "Bloodhoof-US", "Bloodscalp-US", "Bonechewer-US", "Borean Tundra-US", "Boulderfist-US", "Bronzebeard-US", "Burning Blade-US", "Burning Legion-US", "Caelestrasz-US", "Cairne-US", "Cenarion Circle-US", "Cenarius-US", "Cho'gall-US", "Chromaggus-US", "Coilfang-US", "Crushridge-US", "Daggerspine-US", "Dalaran-US", "Dalvengyr-US", "Dark Iron-US", "Darkspear-US", "Darrowmere-US", "Dath'Remar-US", "Dawnbringer-US", "Deathwing-US", "Demon Soul-US", "Dentarg-US", "Destromath-US", "Dethecus-US", "Detheroc-US", "Doomhammer-US", "Draenor-US", "Dragonblight-US", "Dragonmaw-US", "Drak'Tharon-US", "Drak'thul-US", "Draka-US", "Drakkari-US", "Dreadmaul-US", "Drenden-US", "Dunemaul-US", "Durotan-US", "Duskwood-US", "Earthen Ring-US", "Echo Isles-US", "Eitrigg-US", "Eldre'Thalas-US", "Elune-US", "Emerald Dream-US", "Eonar-US", "Eredar-US", "Executus-US", "Exodar-US", "Farstriders-US", "Feathermoon-US", "Fenris-US", "Firetree-US", "Fizzcrank-US", "Frostmane-US", "Frostmourne-US", "Frostwolf-US", "Galakrond-US", "Garithos-US", "Garona-US", "Garrosh-US", "Ghostlands-US", "Gilneas-US", "Gnomeregan-US", "Gorefiend-US", "Gorgonnash-US", "Greymane-US", "Grizzly Hills-US", "Gul'dan-US", "Gundrak-US", "Gurubashi-US", "Hakkar-US", "Haomarush-US", "Hellscream-US", "Hydraxis-US", "Hyjal-US", "Icecrown-US", "Illidan-US", "Jaedenar-US", "Jubei'Thos-US", "Kael'Thas-US", "Kalecgos-US", "Kargath-US", "Kel'Thuzad-US", "Khadgar-US", "Khaz Modan-US", "Khaz'goroth-US", "Kil'Jaeden-US", "Kilrogg-US", "Kirin Tor-US", "Korgath-US", "Korialstrasz-US", "Kul Tiras-US", "Laughing Skull-US", "Lethon-US", "Lightbringer-US", "Lightning's Blade-US", "Lightninghoof-US", "Llane-US", "Lothar-US", "Madoran-US", "Maelstrom-US", "Magtheridon-US", "Maiev-US", "Mal'Ganis-US", "Malfurion-US", "Malorne-US", "Malygos-US", "Mannoroth-US", "Medivh-US", "Misha-US", "Mok'Nathal-US", "Moon Guard-US", "Moonrunner-US", "Mug'thol-US", "Muradin-US", "Nagrand-US", "Nathrezim-US", "Nazgrel-US", "Nazjatar-US", "Ner'zhul-US", "Nesingwary-US", "Nordrassil-US", "Norgannon-US", "Onyxia-US", "Perenolde-US", "Proudmoore-US", "Quel'dorei-US", "Quel'Thalas-US", "Ragnaros-US", "Ravencrest-US", "Ravenholdt-US", "Rexxar-US", "Rivendare-US", "Runetotem-US", "Sargeras-US", "Saurfang-US", "Scarlet Crusade-US", "Scilla-US", "Sen'jin-US", "Sentinels-US", "Shadow Council-US", "Shadowmoon-US", "Shadowsong-US", "Shandris-US", "Shattered Halls-US", "Shattered Hand-US", "Shu'halo-US", "Silver Hand-US", "Silvermoon-US", "Sisters of Elune-US", "Skullcrusher-US", "Skywall-US", "Smolderthorn-US", "Spinebreaker-US", "Spirestone-US", "Staghelm-US", "Steamwheedle Cartel-US", "Stonemaul-US", "Stormrage-US", "Stormreaver-US", "Stormscale-US", "Suramar-US", "Tanaris-US", "Terenas-US", "Terokkar-US", "Thaurissan-US", "The Forgotten Coast-US", "The Scryers-US", "The Underbog-US", "The Venture Co-US", "Thorium Brotherhood-US", "Thrall-US", "Thunderhorn-US", "Thunderlord-US", "Tichondrius-US", "Tortheldrin-US", "Trollbane-US", "Turalyon-US", "Twisting Nether-US", "Uldaman-US", "Uldum-US", "Undermine-US", "Ursin-US", "Uther-US", "Vashj-US", "Vek'nilash-US", "Velen-US", "Warsong-US", "Whisperwind-US", "Wildhammer-US", "Windrunner-US", "Winterhoof-US", "Wyrmrest Accord-US", "Ysera-US", "Ysondre-US", "Zangarmarsh-US", "Zul'jin-US", "Zuluhed-US",
        "Aegwynn-EU", "Aerie Peak-EU", "Agamaggan-EU", "Aggramar-EU", "Ahn'Qiraj-EU", "Al'Akir-EU", "Alexstrasza-EU", "Alleria-EU", "Alonsus-EU", "Aman'Thul-EU", "Ambossar-EU", "Anachronos-EU", "Anetheron-EU", "Antonidas-EU", "Anub'arak-EU", "Arak-arahm-EU", "Arathi-EU", "Arathor-EU", "Archimonde-EU", "Area 52-EU", "Argent Dawn-EU", "Arthas-EU", "Arygos-EU", "Aszune-EU", "Auchindoun-EU", "Azjol-Nerub-EU", "Azshara-EU", "Azuremyst-EU", "Baelgun-EU", "Balnazzar-EU", "Blackhand-EU", "Blackmoore-EU", "Blackrock-EU", "Blade's Edge-EU", "Bladefist-EU", "Bloodfeather-EU", "Bloodhoof-EU", "Bloodscalp-EU", "Blutkessel-EU", "Boulderfist-EU", "Bronze Dragonflight-EU", "Bronzebeard-EU", "Burning Blade-EU", "Burning Legion-EU", "Burning Steppes-EU", "C'thun-EU", "Chamber of Aspects-EU", "Chants éternels-EU", "Cho'gall-EU", "Chromaggus-EU", "Colinas Pardas-EU", "Confrérie du Thorium-EU", "Conseil des Ombres-EU", "Crushridge-EU", "Culte de la Rive Noire-EU", "Daggerspine-EU", "Dalaran-EU", "Dalvengyr-EU", "Darkmoon Faire-EU", "Darksorrow-EU", "Darkspear-EU", "Das Konsortium-EU", "Das Syndikat-EU", "Deathwing-EU", "Defias Brotherhood-EU", "Dentarg-EU", "Der abyssische Rat-EU", "Der Mithrilorden-EU", "Der Rat von Dalaran-EU", "Destromath-EU", "Dethecus-EU", "Die Aldor-EU", "Die Arguswacht-EU", "Die ewige Wacht-EU", "Die Nachtwache-EU", "Die Silberne Hand-EU", "Die Todeskrallen-EU", "Doomhammer-EU", "Draenor-EU", "Dragonblight-EU", "Dragonmaw-EU", "Drak'thul-EU", "Drek'Thar-EU", "Dun Modr-EU", "Dun Morogh-EU", "Dunemaul-EU", "Durotan-EU", "Earthen Ring-EU", "Echsenkessel-EU", "Eitrigg-EU", "Eldre'thalas-EU", "Elune-EU", "Emerald Dream-EU", "Emeriss-EU", "Eonar-EU", "Eredar-EU", "Executus-EU", "Exodar-EU", "Festung der Stürme-EU", "Forscherliga-EU", "Frostmane-EU", "Frostmourne-EU", "Frostwhisper-EU", "Frostwolf-EU", "Garona-EU", "Garrosh-EU", "Genjuros-EU", "Ghostlands-EU", "Gilneas-EU", "Gorgonnash-EU", "Grim Batol-EU", "Gul'dan-EU", "Hakkar-EU", "Haomarush-EU", "Hellfire-EU", "Hellscream-EU", "Hyjal-EU", "Illidan-EU", "Jaedenar-EU", "Kael'Thas-EU", "Karazhan-EU", "Kargath-EU", "Kazzak-EU", "Kel'Thuzad-EU", "Khadgar-EU", "Khaz Modan-EU", "Khaz'goroth-EU", "Kil'Jaeden-EU", "Kilrogg-EU", "Kirin Tor-EU", "Kor'gall-EU", "Krag'jin-EU", "Krasus-EU", "Kul Tiras-EU", "Kult der Verdammten-EU", "La Croisade écarlate-EU", "Laughing Skull-EU", "Les Clairvoyants-EU", "Les Sentinelles-EU", "Lightbringer-EU", "Lightning's Blade-EU", "Lordaeron-EU", "Los Errantes-EU", "Lothar-EU", "Madmortem-EU", "Magtheridon-EU", "Mal'Ganis-EU", "Malfurion-EU", "Malorne-EU", "Malygos-EU", "Mannoroth-EU", "Marécage de Zangar-EU", "Mazrigos-EU", "Medivh-EU", "Minahonda-EU", "Molten Core-EU", "Moonglade-EU", "Mug'thol-EU", "Nagrand-EU", "Nathrezim-EU", "Naxxramas-EU", "Nazjatar-EU", "Nefarian-EU", "Neptulon-EU", "Ner'zhul-EU", "Nera'thor-EU", "Nethersturm-EU", "Nordrassil-EU", "Norgannon-EU", "Nozdormu-EU", "Onyxia-EU", "Outland-EU", "Perenolde-EU", "Proudmoore-EU", "Quel'Thalas-EU", "Ragnaros-EU", "Rajaxx-EU", "Rashgarroth-EU", "Ravencrest-EU", "Ravenholdt-EU", "Rexxar-EU", "Runetotem-EU", "Sanguino-EU", "Sargeras-EU", "Saurfang-EU", "Scarshield Legion-EU", "Sen'jin-EU", "Shadowmoon-EU", "Shadowsong-EU", "Shattered Halls-EU", "Shattered Hand-EU", "Shattrath-EU", "Shen'dralar-EU", "Silvermoon-EU", "Sinstralis-EU", "Skullcrusher-EU", "Spinebreaker-EU", "Sporeggar-EU", "Steamwheedle Cartel-EU", "Stonemaul-EU", "Stormrage-EU", "Stormreaver-EU", "Stormscale-EU", "Sunstrider-EU", "Suramar-EU", "Sylvanas-EU", "Taerar-EU", "Talnivarr-EU", "Tarren Mill-EU", "Teldrassil-EU", "Temple noir-EU", "Terenas-EU", "Terokkar-EU", "Terrordar-EU", "The Maelstrom-EU", "The Sha'tar-EU", "The Venture Co-EU", "Theradras-EU", "Thrall-EU", "Throk'Feroth-EU", "Thunderhorn-EU", "Tichondrius-EU", "Tirion-EU", "Todeswache-EU", "Trollbane-EU", "Turalyon-EU", "Twilight's Hammer-EU", "Twisting Nether-EU", "Tyrande-EU", "Uldaman-EU", "Ulduar-EU", "Uldum-EU", "Un'Goro-EU", "Varimathras-EU", "Vashj-EU", "Vek'lor-EU", "Vek'nilash-EU", "Vol'jin-EU", "Warsong-EU", "Wildhammer-EU", "Wrathbringer-EU", "Xavius-EU", "Ysera-EU", "Ysondre-EU", "Zenedar-EU", "Zirkel des Cenarius-EU", "Zul'jin-EU", "Zuluhed-EU",
        "Азурегос-EU", "Борейская тундра-EU", "Вечная Песня-EU", "Галакронд-EU", "Голдринн-EU", "Гордунни-EU", "Гром-EU", "Дракономор-EU","Король-лич-EU", "Пиратская бухта-EU", "Подземье-EU", "Разувий-EU","Ревущий фьорд-EU", "Свежеватель Душ-EU", "Седогрив-EU", "Страж Смерти-EU", "Термоштепсель-EU", "Ткач Смерти-EU", "Черный Шрам-EU", "Ясеневый лес-EU"
  );
  
  // Using jQuery JSONP module [http://code.google.com/p/jquery-jsonp/].  jQuery's builtin $.getJSON() does not handle errors properly.
  drupal_add_js(drupal_get_path('profile' , 'wowguild_install') . '/js/jquery-jsonp-2.2.1.min.js');
  drupal_add_js(drupal_get_path('profile' , 'wowguild_install') . '/js/wowguild-install-validate-toon.js');
  // This JS is hard coded here to be sure that the code is not cached and/or used on the production site.
  drupal_add_js("
      var all_realms = " . drupal_json_encode($const_realms_list) . ";
      var all_regions = " . drupal_json_encode($all_regions) . ";", 'inline');
  
  $form['character'] = array(
    '#type' => 'fieldset',
    '#title' => 'My World of Warcraft Character',
    '#description' => t('Specify your toon.  Your guild will be determined from armory results.'),
    '#weight' => -1,
  );
  
  $form['character']['name'] = array(
    '#title' => t('Character Name'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => !empty($wowtoon->name) ? $wowtoon->name : NULL,
  );
  $form['character']['realm'] = array(
    '#title' => t('Character Realm'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => !empty($wowtoon->realm) ? $wowtoon->realm : !empty($_SESSION['saved_realm'])?$_SESSION['saved_realm']:'',
  );
    
  $form['#validate'][] = 'wowguild_install_toon_validate';
  $form['#submit'][] = 'wowguild_install_toon_submit';
  
  if (!empty($form_state['toon'])) {
    if (empty($form['site_information']['site_name']['value'])) $form['site_information']['site_name']['value'] = $form_state['toon']->guild_name . ' Guild Site';
    $form['site_information']['site_name']['#default_value'] = $form_state['toon']->guild_name . ' Guild Site';
    $form['admin_account']['account']['name']['#default_value'] = $form_state['toon']->name;
  }
  
}

/**
 * This is called when validating the form.
 * It validates that the toon settings are valid and exist.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function wowguild_install_toon_validate($form, &$form_state) {
  $values = (object) $form_state['values'];
  
  $name = trim($values->name);
  $realm = trim($values->realm);
  
  if (empty($realm)) {
    form_error($form['character']['realm'], 'Character Realm is Required');
  }
  elseif (empty($name)) {
    form_error($form['character']['name'], 'Character Name is Required');
  }
  else {
    
    
    
    $valid_realms = wowguild_get_realms();
    // Replace realm with a valid realm if it matches.  (Forces match in case).
    $valid_realm = wowguild_is_valid_realm($realm);
    if ($valid_realm === FALSE) {
      form_set_error('field_wowtoon_realm', 'Invalid Realm');
    }
    else {
      $form_state['values']['field_wowtoon_realm'][LANGUAGE_NONE][0]['value'] = $valid_realm;
      $avatar = avatar_create(array('type' => 'wowtoon'));
      $avatar->name = $name;
      $avatar->field_wowtoon_realm[LANGUAGE_NONE][0]['value'] = $valid_realm;
    
      // Save last valid realm as a cookie.
      $_SESSION['wowguild_saved_realm'] = array();
      $_SESSION['wowguild_saved_realm']['#value'] = $valid_realm;
    
      // Make sure we are not duplicating.
      $wowtoon = wowtoon_load_by_realm_name($valid_realm, $name);
      
      // Check to see if toon exists in database.
      if (!empty($wowtoon->aid)) {
        if (empty($wowtoon->guild_name)) {
          form_set_error('name', 'Character exists but does not seem to be in a guild.');
        }
        else {
          $form_state['wowtoon'] = $wowtoon;
        }
      }
      else {
        // Toon is not in the database.  Check the Armory
        $validate = $avatar->validateToon();
        
        switch ($validate) {
          case WOWTOON_UPDATE_INVALID_PAGE:
            form_set_error('name', 'Armory returned invalid page.');
            break;
          case WOWTOON_UPDATE_UNKNOWN_ERROR:
            form_set_error('name', 'Armory returned an unexpected error.');
            break;
          case WOWTOON_UPDATE_ARMORY_DOWN:
            form_set_error('name', 'Armory appears to be down.');
            break;
          case WOWTOON_UPDATE_TOO_MANY_REQUESTS:
            form_set_error('name', 'This webpage has made too many requests to the Armory.');
            break;
          case WOWTOON_UPDATE_CHARACTER_NOT_AVAILABLE:
            form_set_error('name', 'This character is not available, or is less than level 10.');
            break;
          case WOWTOON_UPDATE_CHARACTER_NOT_MODIFIED:
          case WOWTOON_UPDATE_CHARACTER_AVAILABLE:
            if (empty($avatar->guild_name)) {
              form_set_error('name', 'Character exists but does not seem to be in a guild.');
            }
            else {
              $form_state['values']['name'] = $avatar->loaded_name;
              $form_state['wowtoon'] = $avatar;
            }
            break;
        }
      }
    }
    
    
  }
}

/**
 * This is only called once upon form submit.
 * It takes ownership of the toon, and sets guild defaults.
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function wowguild_install_toon_submit($form, &$form_state) {
  $toon = $form_state['wowtoon'];
  $toon->uid = 1;
  
  // Create / Load Guild
  $guild = wowguild_create(array(
    'name' => $toon->guild_name,
  ));
  $guild->field_wowtoon_realm = $toon->field_wowtoon_realm;
  wowguild_save($guild);
  
  //print("<pre>");
  //print_r($guild);
//  print_r($toon);
//  die();
  $toon->gid = $guild->gid;
  
  avatar_save($toon);
  //$guild->reloadMembers();
  
  alter_ego_set_current_aid($toon->aid, 1);
  
  
  variable_set('wowguild_guild_name', $toon->guild_name);
  variable_set('wowguild_guild_realm', $toon->realm);
  variable_set('wowguild_guild_gid', $guild->gid);
  
  // Saved menu link id.
  $mlid = variable_get('wowguild_guild_mlid', 0);
  if ($mlid) {
    $ml = menu_link_load($mlid);
    $ml['link_path'] = 'guild/' . $toon->gid;
  }
  else {
    $ml = array(
          'menu_name' => 'main-menu',
          'module' => 'wowguild',
          'link_path' => 'guild/' . $toon->gid,
          'router_path' => 'guild/%',
          'link_title' => t('Guild Roster'),
          'weight' => 5,
    );
  }
  menu_link_save($ml);
  variable_set('wowguild_guild_mlid', $ml['mlid']);
  
  
  
  /*
  // Load Guild For the first time.
  module_load_include('inc','wowtoon','wowtoon.update');
  $guild = wowguild_load($toon->gid);
  $results = wowguild_update_armory($guild->gid);
  if($results->gid) {
    wowguild_update_feed($guild);
    wowguild_update_progression($guild);
  }
  
  $form_state['toon'] = $toon;*/
  
}


/**
* Implements hook_install_tasks().
*
function wowguild_install_install_tasks($install_state) {
  $tasks = array(
    'wowguild_install_load_toons' => array(
      'display_name' => st('Load Guild Members'),
      'type' => 'batch',
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'function' => 'wowguild_install_load_toons'
    ),
  );
  return $tasks;
}

/**
 * TODO: Create batch to load toons.  Also make this step optional in the inital form, or ask here.
 *
function wowguild_install_load_toons() {
  
}
*/
